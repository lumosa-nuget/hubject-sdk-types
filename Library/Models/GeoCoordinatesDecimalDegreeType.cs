using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class GeoCoordinatesDecimalDegreeType
{
    /// <summary>
    /// string that MUST be valid with respect to the following regular expression:
    ///  ^-?1?\d{1,2}\.\d{1,6}$
    /// 
    /// The expression validates the string as a geo coordinate (longitude or latitude) with decimal degree syntax.
    /// Examples: 
    ///     “9.360922”, “-21.568201”
    /// </summary>
    [Required]
    [GraphQLName("Longitude")]
    public string Longitude { get; set; }

    /// <summary>
    /// A string that MUST be valid with respect to the following regular expression:
    /// ^-?1?\d{1,2}\.\d{1,6}$
    ///  
    /// The expression validates the string as a geo coordinate (longitude or latitude) with decimal degree syntax.
    /// Examples: 
    ///     “9.360922”, “-21.568201”
    /// </summary>
    [Required]
    [GraphQLName("Latitude")]
    public string Latitude { get; set; }
}