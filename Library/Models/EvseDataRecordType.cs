using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class EvseDataRecordType
{
    /// <summary>
    /// In case that the operation “PullEvseData” is performed with the parameter “LastCall”, 
    /// Hubject assigns this attribute to every response EVSE record in order to return the changes compared to the last call.
    /// </summary>
    [RegularExpression("Update | Insert | Delete")]
    [GraphQLName("DeltaType")]
    public string DeltaType { get; set; }

    /// <summary>
    /// The attribute indicates the date and time of the last update of the record. 
    /// Hubject assigns this attribute to every response EVSE record.
    /// </summary>
    [GraphQLName("LastUpdate")]
    public string LastUpdate { get; set; }

    /// <summary>
    /// The ID that identifies the charging spot.
    /// </summary>
    [Required]
    [RegularExpression(@"^(([A-Za-z]{2}\*?[A-Za-z0-9]{3}\*?E[A-Za-z0-9\*]{1,30})|(\+?[0-9]{1,3}\*[0-9]{3}\*[0-9\*]{1,32}))$")]
    [GraphQLName("EvseID")]
    public string EvseID { get; set; }

    /// <summary>
    /// The ID that identifies the charging station.
    /// </summary>
    [RegularExpression(@"([A-Za-z]{2}\*?[A-Za-z0-9]{3}\*?P[A-Za-z0-9\*]{1,30})")]
    [GraphQLName("ChargingPoolID")]
    public string ChargingPoolID { get; set; }

    /// <summary>
    /// Name of the charging stations
    /// </summary>
    [Required]
    [GraphQLName("ChargingStationNames")]
    public IEnumerable<InfoTextType> ChargingStationNames { get; set; }

    /// <summary>
    /// Name of the charging point manufacturer
    /// </summary>
    [StringLength(50)]
    [GraphQLName("HardwareManufacturer")]
    public string HardwareManufacturer { get; set; }

    /// <summary>
    /// url that redirect to an online image of the related EVSEID
    /// </summary>
    [StringLength(200)]
    [GraphQLName("ChargingStationImage")]
    public string ChargingStationImage { get; set; }

    /// <summary>
    /// Name of the Sub Operator owning the Charging Station
    /// </summary>
    [StringLength(100)]
    [GraphQLName("SubOperatorName")]
    public string SubOperatorName { get; set; }

    /// <summary>
    /// Address of the charging station.
    /// </summary>
    [Required]
    [GraphQLName("Address")]
    public AddressIso19773Type Address { get; set; }

    /// <summary>
    /// Geolocation of the charging station.
    /// </summary>
    [Required]
    [GraphQLName("GeoCoordinates")]
    public GeoCoordinatesType GeoCoordinates { get; set; }

    /// <summary>
    /// List of plugs that are supported.
    /// </summary>
    [Required]
    [RegularExpression("Small Paddle Inductive|Large Paddle Inductive|AVCON Connector|Tesla Connector|NEMA 5-20|Type E French Standard|Type F Schuko|Type G British Standard|Type J Swiss Standard|Type 1 Connector (Cable Attached)|Type 2 Outlet|Type 2 Connector (Cable Attached)|Type 3 Outlet|IEC 60309 Single Phase|IEC 60309 Three Phase|CCS Combo 2 Plug (Cable Attached)|CCS Combo 1 Plug (Cable Attached)|CHAdeMO")]
    [GraphQLName("Plugs")]
    public IEnumerable<string> Plugs { get; set; }

    /// <summary>
    /// Informs is able to deliver different power outputs.
    /// </summary>
    [GraphQLName("DynamicPowerLevel")]
    public bool? DynamicPowerLevel { get; set; }

    /// <summary>
    /// List of facilities that are supported.
    /// </summary>
    [Required]
    [GraphQLName("ChargingFacilities")]
    public IEnumerable<ChargingFacilityType> ChargingFacilities { get; set; }

    /// <summary>
    /// If the Charging Station provides only renewable energy then the value MUST be” true”, 
    /// if it use grey energy then value MUST be “false”.
    /// </summary>
    [Required]
    [GraphQLName("RenewableEnergy")]
    public bool RenewableEnergy { get; set; }

    /// <summary>
    /// This field gives the information how the charging station provides metering law data.
    /// </summary>
    [Required]
    [RegularExpression("Local|External|Not Available")]
    [GraphQLName("CalibrationLawDataAvailability")]
    public string CalibrationLawDataAvailability { get; set; }

    /// <summary>
    /// List of authentication modes that are supported.
    /// </summary>
    [Required]
    [RegularExpression("NFC RFID Classic|NFC RFID DESFire|PnC|REMOTE|Direct Payment|No Authentication Required")]
    [GraphQLName("AuthenticationModes")]
    public IEnumerable<string> AuthenticationModes { get; set; }

    /// <summary>
    /// This field is used if the EVSE has a limited capacity (e.g. built-in battery). 
    /// Values must be given in kWh.
    /// </summary>
    [GraphQLName("MaxCapacity")]
    public int? MaxCapacity { get; set; }

    /// <summary>
    /// List of payment options that are supported.
    /// </summary>
    [Required]
    [RegularExpression("No Payment|Direct|Contract")]
    [GraphQLName("PaymentOptions")]
    public IEnumerable<string> PaymentOptions { get; set; }

    /// <summary>
    /// List of value added services that are supported.
    /// </summary>
    [Required]
    [RegularExpression("Reservation|DynamicPricing|ParkingSensors|MaximumPowerCharging|PredictiveChargePointUsage|ChargingPlans|RoofProvided|None")]
    [GraphQLName("ValueAddedServices")]
    public IEnumerable<string> ValueAddedServices { get; set; }

    /// <summary>
    /// Specifies how the charging station can be accessed.
    /// 
    /// Free publicly accessible	    EV Driver can reach the charging point without paying a fee, e.g. street, free public place, free parking lot, etc.
    /// Restricted access	            EV Driver needs permission to reach the charging point, e.g. Campus, building complex, etc.
    /// Paying publicly accessible	    EV Driver needs to pay a fee in order to reach the charging point, e.g. payable parking garage, etc.    
    /// Test Station	                Station is just for testing purposes. Access may be restricted.
    /// </summary>
    [Required]
    [RegularExpression("Free publicly accessible|Restricted access|Paying publicly accessible|Test Station")]
    [GraphQLName("Accessibility")]
    public string Accessibility { get; set; }

    /// <summary>
    /// Inform the EV driver where the ChargingPoint could be accessed.
    /// 
    /// OnStreet	                The charging station is located on the street
    /// ParkingLot	                The Charging Point is located inside a Parking Lot
    /// ParkingGarage	            The Charging Point is located inside a Parking Garage
    /// UndergroundParkingGarage	The Charging Point is located inside an Underground Parking Garage
    /// </summary>
    [RegularExpression("OnStreet|ParkingLot|ParkingGarage|UndergroundParkingGarage")] 
    [GraphQLName("AccessibilityLocation")]
    public string AccessibilityLocation { get; set; }

    /// <summary>
    /// Phone number of a hotline of the charging station operator.
    /// regex: ^\+[0-9]{5,15}$ 
    /// </summary>
    [Required]
    [RegularExpression(@"^\+[0-9]{5,15}$")]
    [GraphQLName("HotlinePhoneNumber")]
    public string HotlinePhoneNumber { get; set; }

    /// <summary>
    /// Optional information.
    /// </summary>
    [GraphQLName("AdditionalInfo")]
    public IEnumerable<InfoTextType> AdditionalInfo { get; set; }

    /// <summary>
    /// Last meters information regarding the location of the Charging Station
    /// </summary>
    [GraphQLName("ChargingStationLocationReference")]
    public IEnumerable<InfoTextType> ChargingStationLocationReference { get; set; }

    /// <summary>
    /// In case that the charging spot is part of a bigger facility (e.g. parking place),
    /// this attribute specifies the facilities entrance coordinates.
    /// </summary>
    [GraphQLName("GeoChargingPointEntrance")]
    public GeoCoordinatesType GeoChargingPointEntrance { get; set; }

    /// <summary>
    /// Set in case the charging spot is open 24 hours.
    /// </summary>
    [Required]
    
    [GraphQLName("IsOpen24Hours")]
    public bool IsOpen24Hours { get; set; }

    /// <summary>
    /// Opening time in case that the charging station cannot be accessed around the clock.
    /// </summary>
    [GraphQLName("OpeningTimes")]
    public IEnumerable<OpeningTimesType> OpeningTimes { get; set; }

    /// <summary>
    /// Hub operator
    /// </summary>
    [RegularExpression(@"^(([A-Za-z]{2}\*?[A-Za-z0-9]{3})|(\+?[0-9]{1,3}\*[0-9]{3}))$")]
    [GraphQLName("HubOperatorID")]
    public string HubOperatorID { get; set; }

    /// <summary>
    /// Identification of the corresponding clearing house in the event that roaming between different clearing houses MUST be processed in the future.
    /// </summary>
    [StringLength(20)]
    [GraphQLName("ClearinghouseID")]
    public string ClearinghouseID { get; set; }

    /// <summary>
    /// Is eRoaming via intercharge at this charging station possible? 
    /// If set to "false" the charge spot will not be started/stopped remotely via Hubject.
    /// </summary>
    /// <value></value>
    [Required]
    [GraphQLName("IsHubjectCompatible")]
    public bool IsHubjectCompatible { get; set; }

    /// <summary>
    /// Values: true / false / auto 
    /// 
    /// This attribute indicates whether a CPO provides (dynamic) EVSE Status info in addition to the (static) EVSE Data for this EVSERecord. 
    /// Value auto is set to true by Hubject if the operator offers Hubject EVSEStatus data.
    /// </summary>
    [Required]
    [RegularExpression("True|False|Auto")]
    [GraphQLName("DynamicInfoAvailable")]
    public string DynamicInfoAvailable { get; set; }
}