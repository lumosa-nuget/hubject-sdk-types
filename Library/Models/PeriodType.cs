using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class PeriodType
{
    [Required]
    [RegularExpression(@"[0-9]{2}:[0-9]{2}")]
    [GraphQLName("Begin")]
    public string Begin { get; set; }

    [Required]
    [RegularExpression(@"[0-9]{2}:[0-9]{2}")]
    [GraphQLName("End")]
    public string End { get; set; }
}