using HotChocolate;

public class GeoCoordinatesGoogleType
{
    /// <summary>
    /// string that MUST be valid with respect to the following regular expression:
    /// ^-?1?\d{1,2}\.\d{1,6}\s*\,?\s*-?1?\d{1,2}\.\d{1,6}$
    /// 
    /// The expression validates the string as geo coordinates with respect to the Google standard. 
    /// The string contains latitude and longitude (in this sequence) separated by a space.
    /// </summary>
    [GraphQLName("Coordinatea")]
    public string Coordinatea { get; set; }
}
