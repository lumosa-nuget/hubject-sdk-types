using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class GeoCoordinatesDegreeMinuteSecondsType
{
    /// <summary>
    /// A string that MUST be valid with respect to the following regular expression:
    /// ^-?1?\d{1,2}°[ ]?\d{1,2}'[ ]?\d{1,2}\.\d+''$
    /// 
    /// The expression validates the string as a geo coordinate (longitude or latitude) consisting of degree, minutes, and seconds
    /// Examples: 
    ///     “9°21'39.32''”, “-21°34'23.16''
    /// </summary>
    [Required]
    [GraphQLName("Longitude")]
    public string Longitude { get; set; }

    /// <summary> 
    /// A string that MUST be valid with respect to the following regular expression:
    /// ^-?1?\d{1,2}°[ ]?\d{1,2}'[ ]?\d{1,2}\.\d+''$
    ///  
    /// The expression validates the string as a geo coordinate (longitude or latitude) consisting of degree, minutes, and seconds
    /// Examples: 
    ///     “9°21'39.32''”, “-21°34'23.16''
    /// </summary>
    [Required]
    [GraphQLName("Latitude")]
    public string Latitude { get; set; }
}