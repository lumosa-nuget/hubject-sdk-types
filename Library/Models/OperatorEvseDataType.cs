using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class OperatorEvseDataType
{
    /// <summary>
    /// The provider whose data records are listed below
    /// </summary>
    [Required]
    [RegularExpression(@"^(([A-Za-z]{2}\*?[A-Za-z0-9]{3})|(\+?[0-9]{1,3}\*[0-9]{3}))$")]
    [GraphQLName("OperatorID")]
    public string OperatorID { get; set; }

    /// <summary>
    /// Free text for operator
    /// </summary>
    [Required]
    [GraphQLName("OperatorName")]
    public string OperatorName { get; set; }

    /// <summary>
    /// Charger entries to be registered
    /// </summary>
    [Required]
    [GraphQLName("EvseDataRecord")]
    public EvseDataRecordType[] EvseDataRecord { get; set; }
};