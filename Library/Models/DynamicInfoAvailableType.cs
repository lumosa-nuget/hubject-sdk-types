using System.Runtime.Serialization;

public enum DynamicInfoAvailableType 
{
    [EnumMember(Value = "True")]
    True,

    [EnumMember(Value = "False")]
    False,

    [EnumMember(Value = "Auto")] 
    Auto
}