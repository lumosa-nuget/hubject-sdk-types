using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class OpeningTimesType
{
    /// <summary>
    /// The starting and end time for pricing product applicability in the specified period
    /// </summary>
    [Required]
    [GraphQLName("Period")]
    public PeriodType Period { get; set; }

    /// <summary>
    /// Day values to be used in specifying periods on which the product is available. 
    ///     Workdays = Monday – Friday, 
    ///     Weekend = Saturday – Sunday
    /// </summary>
    [Required]
    [GraphQLName("On")]
    public string On { get; set; }
}