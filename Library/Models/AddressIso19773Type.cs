using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class AddressIso19773Type 
{
    /// <summary>
    /// The CountryCodeType allows for Alpha-3 country codes only as of OICP 2.2 and OICP 2.3
    /// For Alpha-3 (three-letter) country codes as defined in ISO 3166-1.
    /// </summary>
    [Required]
    [GraphQLName("Country")]
    public string Country { get; set; }

    [Required]
    [StringLength(50, MinimumLength = 1)]
    [GraphQLName("City")]
    public string City { get; set; }

    [Required]
    [StringLength(100, MinimumLength = 2)]
    [GraphQLName("Street")]
    public string Street { get; set; }

    [Required]
    [StringLength(10)]
    [GraphQLName("PostalCode")]
    public string PostalCode { get; set; }

    [Required]
    [StringLength(10)]
    [GraphQLName("HouseNum")]
    public string HouseNum { get; set; }

    [GraphQLName("Floor")]
    public string Floor { get; set; }

    [StringLength(50)]
    [GraphQLName("Region")]
    public string Region { get; set; }

    [GraphQLName("ParkingFacility")]
    public bool? ParkingFacility { get; set; }

    [StringLength(5)]
    [GraphQLName("ParkingSpot")]
    public string ParkingSpot { get; set; }

    /// <summary>
    /// The expression validates a string as a Time zone with UTC offset.
    /// regex: [U][T][C][+,-][0-9][0-9][:][0-9][0-9]
    /// Examples:
    ///     UTC+01:00
    ///     UTC-05:00
    /// </summary>
    /// <value></value>
    [GraphQLName("TimeZone")]
    public string TimeZone { get; set; }
}