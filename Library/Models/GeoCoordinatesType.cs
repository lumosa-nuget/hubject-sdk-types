using HotChocolate;

public class GeoCoordinatesType 
{
    /// <summary>
    /// Geocoordinates using Google Structure
    /// </summary>
    [GraphQLName("Google")]
    public GeoCoordinatesGoogleType Google { get; set; }

    /// <summary>
    /// Geocoordinates using DecimalDegree Structure
    /// </summary>
    [GraphQLName("DecimalDegree")]
    public GeoCoordinatesDecimalDegreeType DecimalDegree {get; set;}

    /// <summary>
    /// Geocoordinates using DegreeMinutesSeconds Structure
    /// </summary>
    [GraphQLName("DegreeMinuteSeconds")]
    public GeoCoordinatesDegreeMinuteSecondsType DegreeMinuteSeconds {get; set;}

}