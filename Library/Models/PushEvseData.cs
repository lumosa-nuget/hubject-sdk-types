using System.ComponentModel.DataAnnotations;
using HotChocolate;
/// <summary>
/// eRoamingPushEvseData is a message that is sent in order to upload EVSE data to the Hubject system
/// </summary>
public class PushEvseData
{
    /// <summary>
    /// Describes the action that has to be performed by Hubject with the provided data.
    /// </summary>
    [Required]
    [RegularExpression("FullLoad | Update | Insert | Delete")]
    [GraphQLName("ActionType")]
    public string ActionType { get; set;}

    [Required]
    [GraphQLName("OperatorEvseData")]
    public OperatorEvseDataType OperatorEvseData { get; set;}
}