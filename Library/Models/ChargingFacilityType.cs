using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class ChargingFacilityType
{
    /// <summary>
    ///  Charging Facility power type (e.g. AC or DC)
    ///         'AC_1_PHASE' | 'AC_3_PHASE' | 'DC';
    /// </summary>
    [Required]
    [GraphQLName("PowerType")]
    public string PowerType { get; set; }

    /// <summary>
    /// Voltage (Line to Neutral) of the Charging Facility
    /// </summary>
    [GraphQLName("Voltage")]
    public int? Voltage { get; set; }

    /// <summary>
    /// Amperage of the Charging Facility
    /// </summary>
    [GraphQLName("Amperage")]
    public int? Amperage { get; set; }

    /// <summary>
    /// Charging Facility power in kW
    /// </summary>
    [Required]
    [GraphQLName("Power")]
    public int Power { get; set; }

    /// <summary>
    /// List of charging modes that are supported.
    /// </summary>
    [RegularExpression("Mode_1|Mode_2|Mode_3|Mode_4|CHAdeMO")]
    [GraphQLName("ChargingModes")]
    public string ChargingModes { get; set; }
}