using System.ComponentModel.DataAnnotations;
using HotChocolate;

public class InfoTextType
{
    [Required]
    [RegularExpression(@"^[a-z]{2,3}(?:-[A-Z]{2,3}(?:-[a-zA-Z]{4})?)?(?:-x-[a-zA-Z0-9]{1,8})?$")]
    [GraphQLName("lang")]
    public string Lang { get; set; }

    [Required]
    [GraphQLName("value")]
    public string Value { get; set; }
}